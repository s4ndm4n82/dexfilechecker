#!/bin/env bash

################################################################################
#                                                                              #
#                        dexFileChecker for EndeavourOS                        #
#             This script will enable dex to execut *.desktop files            #
#             from xdg/autostart folder and from user autostart folder.        #
#                                                                              #
# Created by s4ndm4n                                                   v 0.0.1 #
################################################################################

#Read all files from user autostart folder.
declare -a fileNameListHome=( $HOME/.config/autostart/*.desktop )

#Load the files in /etc/xdg/autostart to the nammed array.
declare -a fileNameListUsr=( /etc/xdg/autostart/* )

#Loop match and execute desktop files from /usr/xdg/autostart.
for fn in "${fileNameListUsr[@]}";
    do
        case "${fn##*/}" in     #Start mathing file names.
            xfce*.desktop | xfset*.desktop)     ;; #Match files with xfce and xfset.
            *)  echo "dex ${fn##*/}"            ;; #This will run all the other files.
        
        esac
    done

#Loop to execute desktop files in ~/.config/autostart.
for uFn in "${fileNameListHome[@]}";
    do
        echo "dex ${uFn##*/}"
    done